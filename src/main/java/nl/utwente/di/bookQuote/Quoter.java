package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    double getBookPrice(String isbn) {
        while (true) {
            switch (isbn) {
                case "1":
                    return 10.0;

                case "2":
                    return 45.0;

                case "3":
                    return 20.0;

                case "4":
                    return 35.0;

                case "5":
                    return 50.0;

                case "others":
                    return 0.0;

                default:
                    System.out.println("Error");
            }
        }
    }
}
